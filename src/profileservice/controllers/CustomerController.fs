﻿namespace Customer.Controllers

open System
open System.Web.Http
open Customer.CPCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("CP")>]
type creditCardController() =
    inherit ApiController()

    [<Route("Search")>]
    member this.Search (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        Search(jsonContent)

    [<Route("Login")>]
    member this.Login (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        Login(jsonContent)

    [<Route("CustomerByID")>]
    member this.CustomerByID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetCustomerByID(jsonContent)

    [<Route("InsertNewCustomer")>]
    member this.InsertNewCustomer (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        InsertNewCustomer(jsonContent)

    [<Route("UpdateCustomer")>]
    member this.UpdateCustomer (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        UpdateCustomer(jsonContent)
