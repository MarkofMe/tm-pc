﻿namespace Staff.Controllers

open System
open System.Web.Http
open Staff.SPCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("SP")>]
type staffProfileController() =
    inherit ApiController()

    [<Route("All")>]
    member this.All (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("Login")>]
    member this.Login (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        Login(jsonContent)

    [<Route("StaffByID")>]
    member this.StaffByID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetStaffByID(jsonContent)

    [<Route("StaffByDealership")>]
    member this.StaffByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetByDealership(jsonContent)

    [<Route("InsertNewStaff")>]
    member this.InsertNewStaff (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        InsertNewStaff(jsonContent)

    [<Route("UpdateStaff")>]
    member this.UpdateStaff (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        UpdateStaff(jsonContent)

    [<Route("RemoveStaff")>]
    member this.RemoveStaff (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        DeleteStaff(jsonContent)