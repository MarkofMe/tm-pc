namespace Customer

open System
open FSharp.Core

module CPCodeTypes =
    
    type staffAuth = {
                        UserName : string
                        Password : string
                        }

    type position = {
                    Position : string
                    }

    type customerSearch = {
                            StaffUserName : string
                            StaffPassword : string
                            FirstName : string
                            LastName : string
                            Town : string
                            Postcode : string
                            }

    type customerData = {
                        CustomerUserName : string
                        CustomerID : Nullable<int>
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Status : string
                    }

    type customerDataCheck = {
                        CustomerUserName : string
                        CustomerID : Nullable<int>
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Active : Nullable<bool>
                        Status : string
                    }

    type displayCustomerData = {
                        CustomerUserName : string
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                    }

    type customerLogin = {
                            CustomerUserName: string
                            CustomerPassword: string
                            }

    type customerLoginData = {
                            CustomerUserName: string
                            CustomerPassword: string
                            CustomerID: Nullable<int>
                            }
    
    type customerID = {
                            StaffUserName : string
                            StaffPassword : string
                            CustomerID: Nullable<int>
                            }

    type insertData = {
                        CustomerUserName: string
                        CustomerPassword: string
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        }

    type updateData = {
                        CustomerID: int
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Active : bool
                        }