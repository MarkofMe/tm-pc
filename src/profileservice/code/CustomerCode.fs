namespace Customer

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open Newtonsoft.Json
open Customer.CPCodeTypes
open System.Net.Mail

module CPCode =
    
    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptydisplayCustomerData = {
                                    CustomerUserName = ""
                                    FirstName =""
                                    LastName =""
                                    Email =""
                                    HouseNumber = ""
                                    Address1 =""
                                    Address2 =""
                                    Town =""
                                    Postcode =""
                                    }

    let emptyCustomerData = {
                                CustomerUserName = ""
                                CustomerID = nullable 0
                                FirstName =""
                                LastName =""
                                Email =""
                                HouseNumber = ""
                                Address1 =""
                                Address2 =""
                                Town =""
                                Postcode =""
                                Status = "WRONG"
                                }

    let emptySearchParam = {
                            StaffUserName = ""
                            StaffPassword = ""
                            FirstName = ""
                            LastName = ""
                            Town = ""
                            Postcode = ""
                            }

    let AuthCheck (creds : staffAuth) = 
        (query {
            for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                    select row
                } |> Seq.map (fun row -> {
                                            Position = row.Position
                                            }) |> Seq.toArray).[0]

    let accountCheck (creds : customerLogin) =
        query {
            for row in schema.CustomerLoginCustomerUserNameCheck(creds.CustomerUserName) do
            select row
        } |> Seq.map (fun row -> {
                                    CustomerUserName = row.CustomerUserName
                                    CustomerID = row.CustomerID
                                    FirstName = row.FirstName
                                    LastName = row.LastName
                                    Email = row.Email
                                    HouseNumber = row.HouseNumber
                                    Address1 = row.Address1
                                    Address2 = row.Address2
                                    Town = row.Town_City
                                    Postcode = row.Postcode
                                    Active = row.Active
                                    Status = "OK"
                                    })

    let Search(searchParam : string) =
        try
            let search = try
                            JsonConvert.DeserializeObject<customerSearch>(searchParam)
                         with ex ->
                            emptySearchParam

            let creds = 
                            {
                                UserName = search.StaffUserName
                                Password = search.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    query {
                        for row in schema.CustomerSearch(search.FirstName,search.LastName,search.Town,search.Postcode) do
                        select row
                    } |> Seq.map (fun row -> {
                                                CustomerUserName = row.CustomerUserName
                                                CustomerID = row.CustomerID
                                                FirstName = row.FirstName
                                                LastName = row.LastName
                                                Email = row.Email
                                                HouseNumber = row.HouseNumber
                                                Address1 = row.Address1
                                                Address2 = row.Address2
                                                Town = row.Town_City
                                                Postcode = row.Postcode
                                                Status = "OK"
                                                })|> Seq.toArray
            |_ -> [|emptyCustomerData|]
        with ex ->
            [|emptyCustomerData|]

    let Login(userCreds: string) =
        try
            let creds = try
                             JsonConvert.DeserializeObject<customerLogin>(userCreds)
                         with ex ->
                             {
                                CustomerUserName = ""
                                CustomerPassword = ""
                             }
            let query =
                query {
                    for row in schema.CustomerLoginCustomer(creds.CustomerUserName, (hash creds.CustomerPassword).ToString()) do
                    select row
                } |> Seq.map (fun row -> {
                                            CustomerUserName = row.CustomerUserName
                                            CustomerID = row.CustomerID
                                            FirstName = row.FirstName
                                            LastName = row.LastName
                                            Email = row.Email
                                            HouseNumber = row.HouseNumber
                                            Address1 = row.Address1
                                            Address2 = row.Address2
                                            Town = row.Town_City
                                            Postcode = row.Postcode
                                            Status = "OK"
                                            })
            let returnData = query |> Seq.toArray

            match returnData.Length with
            | 1 -> returnData
            |_-> let query2 = accountCheck(creds)
                 let returnData2 = query2 |> Seq.toArray

                 match returnData2.Length with
                 | 1 -> let a = returnData2.GetValue(0) :?> customerDataCheck
                        match a.Active.Value with
                        | true -> [|{
                                    CustomerUserName = ""
                                    CustomerID = nullable 0
                                    FirstName =""
                                    LastName =""
                                    Email =""
                                    HouseNumber = ""
                                    Address1 =""
                                    Address2 =""
                                    Town =""
                                    Postcode =""
                                    Status = "PASSWORD"
                                    }|]
                        |_-> [|{
                                CustomerUserName = ""
                                CustomerID = nullable 0
                                FirstName =""
                                LastName =""
                                Email =""
                                HouseNumber = ""
                                Address1 =""
                                Address2 =""
                                Town =""
                                Postcode =""
                                Status = "INACTIVE"
                                }|]
                 |_ -> [|emptyCustomerData|]

        with ex ->
            [|emptyCustomerData|]

    let GetCustomerByID(id: string) =
        try
            let id = try
                             JsonConvert.DeserializeObject<customerID>(id)
                         with ex ->
                             {
                                StaffUserName = ""
                                StaffPassword = ""
                                CustomerID = nullable 0
                             }
            
            let creds = 
                            {
                                UserName = id.StaffUserName
                                Password = id.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    
                    (query {
                        for row in schema.CustomerGetByID(id.CustomerID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                CustomerUserName = row.CustomerUserName
                                                CustomerID = row.CustomerID
                                                FirstName = row.FirstName
                                                LastName = row.LastName
                                                Email = row.Email
                                                HouseNumber = row.HouseNumber
                                                Address1 = row.Address1
                                                Address2 = row.Address2
                                                Town = row.Town_City
                                                Postcode = row.Postcode
                                                Status = "OK"
                                                }) |> Seq.toArray).[0]
            |_ -> emptyCustomerData
        with ex ->
            emptyCustomerData

    let InsertNewCustomer (data : string) =
        try
            let newCustomer = JsonConvert.DeserializeObject<insertData>(data)
            
            schema.CustomerInsert(newCustomer.CustomerUserName, hash(newCustomer.CustomerPassword).ToString(), newCustomer.FirstName, newCustomer.LastName, newCustomer.Email, newCustomer.HouseNumber, newCustomer.Address1, newCustomer.Address2, newCustomer.Town, newCustomer.Postcode) |> ignore
            
            let smtp = new SmtpClient(
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential("teesmo.messages@gmail.com", "TTx5!0PSM69HTz"))
    
            let message = new MailMessage("teesmo.messages@gmail.com", newCustomer.Email)
            message.Subject <- "Welcome to TeesMo"
            message.Body <- "<p>Hello " + newCustomer.FirstName + " " + newCustomer.LastName+"</p><br/> <p>Thank you for creating an account with Teesside Motors.</p><p>You now have the capability to book test drives and car services.</p><p>We hope to see you soon.</p><p>Teesside Motors!</p>"
            message.IsBodyHtml <- true

            smtp.Send(message)
            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError

    let UpdateCustomer (data : string) =
        try
            let Customer = JsonConvert.DeserializeObject<updateData>(data)
            
            schema.CustomerUpdate(nullable Customer.CustomerID, Customer.FirstName, Customer.LastName, Customer.Email, Customer.HouseNumber, Customer.Address1, Customer.Address2, Customer.Town, Customer.Postcode, nullable Customer.Active) |> ignore

            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError
            