namespace Staff

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text
open Newtonsoft.Json

module SPTypes =

    type staffAuth = {
                        UserName : string
                        Password : string
                        }

    type position = {
                    Position : string
                    }

    type staffByDealership = {
                            StaffUserName : string
                            StaffPassword : string
                            Dealership : string
                            }

    type staffData = {
                        StaffUserName : string
                        StaffID : Nullable<int>
                        StaffName : string
                        DoB : string
                        Email : string
                        Address : string
                        Postcode : string
                        Position : string
                        Salary : Nullable<float>
                        DealershipName : string
                        Status : string
                    }

    type staffLoginReturnData = {
                        StaffUserName : string
                        StaffID : Nullable<int>
                        firstName : string
                        lastName : string
                        DoB : string
                        Email : string
                        HouseNumber : Nullable<int16>
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Position : string
                        Salary : Nullable<float>
                        DealershipName : string
                        Status : string
                    }

    type staffDataCheck = {
                        StaffUserName : string
                        StaffID : Nullable<int>
                        FirstName : string
                        LastName : string
                        DoB : string
                        Email : string
                        HouseNumber : Nullable<int16>
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Position : string
                        Salary : Nullable<float>
                        DealershipName : string
                        Active : Nullable<bool>
                        Status : string
                    }

    type displayStaffData = {
                        StaffUserName : string
                        FirstName : string
                        LastName : string
                        DoB : string
                        Email : string
                        HouseNumber : Nullable<int>
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Position : string
                        Salary : Nullable<float>
                        DealershipName : string
                    }

    type staffLogin = {
                            StaffUserName: string
                            StaffPassword: string
                            }

    type staffLoginData = {
                            StaffUserName: string
                            StaffPassword: string
                            StaffID: Nullable<int>
                            }

    type insertData = {
                        StaffUserName: string
                        StaffPassword: string
                        NewStaffUserName: string
                        NewStaffPassword: string
                        FirstName : string
                        LastName : string
                        DoB : string
                        Email : string
                        HouseNumber : Nullable<int>
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Position : string
                        Salary : Nullable<float>
                        DealershipName : string
                        }

    type updateData = {
                        StaffUserName: string
                        StaffPassword: string
                        StaffID: Nullable<int>
                        FirstName : string
                        LastName : string
                        DoB : string
                        Email : string
                        HouseNumber : Nullable<int>
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Position : string
                        Salary : Nullable<float>
                        DealershipName : string
                        }