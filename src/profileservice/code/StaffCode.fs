namespace Staff

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text
open Newtonsoft.Json
open Staff.SPTypes

module SPCode =
    
    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptydisplayStaffData = {
                                    StaffUserName = ""
                                    FirstName = ""
                                    LastName = ""
                                    DoB = ""
                                    Email = ""
                                    HouseNumber = nullable 0
                                    Address1 = ""
                                    Address2 = ""
                                    Town = ""
                                    Postcode = ""
                                    Position = ""
                                    Salary = nullable 0.0
                                    DealershipName = ""
                                    }

    let emptyStaffData = {
                                StaffUserName = ""
                                StaffID = nullable 0
                                StaffName = ""
                                DoB = ""
                                Email = ""
                                Address = ""
                                Postcode = ""
                                Position = ""
                                Salary = nullable 0.0
                                DealershipName = ""
                                Status = "WRONG"
                                }

    let emptyStaffReturnData = {
                                StaffUserName = ""
                                StaffID = nullable 0
                                firstName = ""
                                lastName = ""
                                DoB = ""
                                Email = ""
                                HouseNumber = nullable 0s
                                Address1 = ""
                                Address2 = ""
                                Town = ""
                                Postcode = ""
                                Position = ""
                                Salary = nullable 0.0
                                DealershipName = ""
                                Status = "WRONG"
                                }

    let emptySearchParam = {
                            StaffUserName = ""
                            StaffPassword = ""
                            Dealership = ""
                            }

    let AuthCheck (creds : staffAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]

    let accountCheck (creds : staffLogin) =
        query {
            for row in schema.StaffLoginStaffUserNameCheck(creds.StaffUserName) do
            select row
        } |> Seq.map (fun row -> {
                                            StaffUserName = row.StaffUserName
                                            StaffID = row.StaffId
                                            FirstName = row.FirstName
                                            LastName = row.LastName
                                            DoB = row.DateOfBirth
                                            Email = row.Email
                                            HouseNumber = row.HouseNumber
                                            Address1 = row.Address1
                                            Address2 = row.Address2
                                            Town = row.Town_City
                                            Postcode = row.PostCode
                                            Position = row.Position
                                            Salary = row.Salary
                                            DealershipName = row.Name
                                            Active = row.Active
                                            Status = "OK"
                                            })

    let GetAll(searchParam : string) =
        try
            let search = try
                            JsonConvert.DeserializeObject<staffAuth>(searchParam)
                         with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let creds = 
                            {
                                UserName = search.UserName
                                Password = search.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                query {
                    for row in schema.StaffGetAll() do
                    select row
                } |> Seq.map (fun row -> 
                                            {
                                                StaffUserName = row.StaffUserName
                                                StaffID = row.StaffId
                                                StaffName = row.StaffName
                                                DoB = row.DateOfBirth
                                                Email = row.Email
                                                Address = row.Address
                                                Postcode = row.PostCode
                                                Position = row.Position
                                                Salary = row.Salary
                                                DealershipName = row.Name
                                                Status = "OK"
                                            }) |> Seq.toArray
            |_ -> [|emptyStaffData|]
        with ex ->
            [|emptyStaffData|]
    
    let GetByDealership(searchParam : string) =
        try
            let search = try
                            JsonConvert.DeserializeObject<staffByDealership>(searchParam)
                         with ex ->
                            {
                                StaffUserName = ""
                                StaffPassword = ""
                                Dealership = ""
                            }

            let creds = 
                            {
                                UserName = search.StaffUserName
                                Password = search.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                query {
                    for row in schema.StaffGetByDealership(search.Dealership) do
                    select row
                } |> Seq.map (fun row -> 
                                            {
                                                StaffUserName = row.StaffUserName
                                                StaffID = row.StaffId
                                                StaffName = row.StaffName
                                                DoB = row.DateOfBirth
                                                Email = row.Email
                                                Address = row.Address
                                                Postcode = row.PostCode
                                                Position = row.Position
                                                Salary = row.Salary
                                                DealershipName = row.Name
                                                Status = "OK"
                                            }) |> Seq.toArray
            |_ -> [|emptyStaffData|]
        with ex ->
            [|emptyStaffData|]

    let Login(userCreds: string) =
        try
            let creds = try
                             JsonConvert.DeserializeObject<staffLogin>(userCreds)
                         with ex ->
                             {
                                StaffUserName = ""
                                StaffPassword = ""
                             }
            let query =
                query {
                    for row in schema.StaffLoginStaff(creds.StaffUserName, (hash creds.StaffPassword).ToString()) do
                    select row
                } |> Seq.map (fun row -> {
                                                StaffUserName = row.StaffUserName
                                                StaffID = row.StaffId
                                                firstName = row.FirstName
                                                lastName = row.LastName
                                                DoB = row.DateOfBirth
                                                Email = row.Email
                                                HouseNumber = row.HouseNumber
                                                Address1 = row.Address1
                                                Address2 = row.Address2
                                                Town = row.Town_City
                                                Postcode = row.PostCode
                                                Position = row.Position
                                                Salary = row.Salary
                                                DealershipName = row.Name
                                                Status = "OK"
                                            })
            let returnData = query |> Seq.toArray

            match returnData.Length with
            | 1 -> returnData
            |_-> let query2 = accountCheck(creds)
                 let returnData2 = query2 |> Seq.toArray

                 match returnData2.Length with
                 | 1 -> let a = returnData2.GetValue(0) :?> staffDataCheck
                        match a.Active.Value with
                        | true -> [|{
                                        StaffUserName = ""
                                        StaffID = nullable 0
                                        firstName = ""
                                        lastName = ""
                                        DoB = ""
                                        Email = ""
                                        HouseNumber = nullable 0s
                                        Address1 = ""
                                        Address2 = ""
                                        Town = ""
                                        Postcode = ""
                                        Position = ""
                                        Salary = nullable 0.0
                                        DealershipName = ""
                                        Status = "PASSWORD"
                                        }|]
                        |_-> [|{
                                        StaffUserName = ""
                                        StaffID = nullable 0
                                        firstName = ""
                                        lastName = ""
                                        DoB = ""
                                        Email = ""
                                        HouseNumber = nullable 0s
                                        Address1 = ""
                                        Address2 = ""
                                        Town = ""
                                        Postcode = ""
                                        Position = ""
                                        Salary = nullable 0.0
                                        DealershipName = ""
                                        Status = "INACTIVE"
                                        }|]
                 |_ -> [|emptyStaffReturnData|]
        with ex ->
            [|emptyStaffReturnData|]

    let GetStaffByID(data: string) =
        try
            let id = try
                            JsonConvert.DeserializeObject<staffLoginData>(data)
                         with ex ->
                            {
                                StaffUserName = ""
                                StaffPassword = ""
                                StaffID = nullable 0
                            }

            let creds = 
                            {
                                UserName = id.StaffUserName
                                Password = id.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management")with
            | true ->
                    query {
                        for row in schema.StaffGetByID(id.StaffID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                StaffUserName = row.StaffUserName
                                                StaffID = row.StaffId
                                                StaffName = row.StaffName
                                                DoB = row.DateOfBirth
                                                Email = row.Email
                                                Address = row.Address
                                                Postcode = row.PostCode
                                                Position = row.Position
                                                Salary = row.Salary
                                                DealershipName = row.Name
                                                Status = "OK"
                                            }) |> Seq.toArray
            |_-> [|emptyStaffData|]
        with ex ->
            [|emptyStaffData|]

    let InsertNewStaff (data : string) =
        try
            let newStaff = try
                            JsonConvert.DeserializeObject<insertData>(data)
                           with ex ->
                            {
                                StaffUserName = ""
                                StaffPassword = ""
                                NewStaffUserName = ""
                                NewStaffPassword = ""
                                FirstName = ""
                                LastName = ""
                                DoB = ""
                                Email = ""
                                HouseNumber = nullable 0
                                Address1 = ""
                                Address2 = ""
                                Town = ""
                                Postcode = ""
                                Position = ""
                                Salary = nullable 0.0
                                DealershipName = ""
                            }

            let creds = 
                            {
                                UserName = newStaff.StaffUserName
                                Password = newStaff.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management")with
            | true ->
                        schema.StaffInsert(newStaff.NewStaffUserName, newStaff.NewStaffPassword, newStaff.FirstName, newStaff.LastName, newStaff.DoB, newStaff.Email, newStaff.HouseNumber, newStaff.Address1, newStaff.Address2, newStaff.Town, newStaff.Postcode, newStaff.Position, newStaff.Salary, newStaff.DealershipName) |> ignore
                        HttpStatusCode.OK
            |_-> HttpStatusCode.Unauthorized
        with ex ->
            HttpStatusCode.InternalServerError

    let UpdateStaff (data : string) =
        try
            let Staff = try
                                JsonConvert.DeserializeObject<updateData>(data)
                           with ex ->
                                {
                                    StaffUserName = ""
                                    StaffPassword = ""
                                    StaffID = nullable 0
                                    FirstName = ""
                                    LastName = ""
                                    DoB = ""
                                    Email = ""
                                    HouseNumber = nullable 0
                                    Address1 = ""
                                    Address2 = ""
                                    Town = ""
                                    Postcode = ""
                                    Position = ""
                                    Salary = nullable 0.0
                                    DealershipName = ""
                                }

            let creds = 
                            {
                                UserName = Staff.StaffUserName
                                Password = Staff.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    schema.StaffUpdate(Staff.StaffID, Staff.FirstName, Staff.LastName, Staff.DoB, Staff.Email, Staff.HouseNumber, Staff.Address1, Staff.Address2, Staff.Town, Staff.Postcode, Staff.Position, Staff.Salary, Staff.DealershipName) |> ignore
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Unauthorized
        with ex ->
            HttpStatusCode.InternalServerError

    let DeleteStaff (data : string) =
        try
            let Staff = try
                                JsonConvert.DeserializeObject<staffLoginData>(data)
                           with ex ->
                                {
                                    StaffUserName = ""
                                    StaffPassword = ""
                                    StaffID = nullable 0
                                }

            let creds = 
                            {
                                UserName = Staff.StaffUserName
                                Password = Staff.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                    schema.StaffDelete(Staff.StaffID) |> ignore
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Unauthorized
        with ex ->
            HttpStatusCode.InternalServerError
            